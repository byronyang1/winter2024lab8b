import java.util.Scanner;
public class BoardGameApp {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		//Creates our board object for our game
		Board board = new Board();
		System.out.println("Welcome user!");
		
		//Sets our number of castle to place to 7 and the amount of turns we have played (starting at 0)
		int numCastles = 7;
		int turns = 0;
		
		//Our game keeps running while we still have castles to place and we have less than 8 turns played
		while (numCastles > 0 && turns < 8) {
			//Prints our board at the start of each round
			System.out.println(board);
			
			//We tell the user how many castles they have left to place and how many turns they've played
			System.out.println("Number of castles left to place: " + numCastles);
			System.out.println("Amount of turns played: " + turns); 
			
			//We prompt the user to enter a row and column to place their castle
			System.out.println("Please enter 2 integers representing the row and column of the board (0-4 inclusively): ");
			int row = sc.nextInt();
			int column = sc.nextInt();
			int x = board.placeToken(row, column);
			
			//If their row or column is outside of the board index range than they're prompted to enter valid values
			while (x < 0) {
				System.out.println("Please re-enter valid values representing the row and column of the board (0-4 inclusively): ");
				row = sc.nextInt();
				column = sc.nextInt();
				x = board.placeToken(row, column);
			}
			
			//If the user's set position to place a castle has a wall. The wall will appear and their turn will increase. The castle will fail to be placed 
			if (x == 1) {
				System.out.println("There was a wall at your declared position!");
				turns++;
			}
			
			//If the set position is blank, than the user will know a castle was successfully placed. We then increase their turn and the number of castles they have left to place
			if (x == 0) {
				System.out.println("A castle was successfully placed!");
				turns++;
				numCastles--;
			}
		}
		
		//Prints the board at the end of the game
		System.out.println(board);
		
		//If they have no more castles to place, they've won the game, otherwhise they have lost!
		if (numCastles == 0) {
			System.out.println("Congratulations! You won the game!");
		} else {
			System.out.println("Too bad! You lost the game!");
		}
	}
}