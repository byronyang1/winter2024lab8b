import java.util.Random;
public class Board {
	private Tile[][] grid;
	private final int SIZEBOARD = 5;
	public Board() {
		this.grid = new Tile[this.SIZEBOARD][this.SIZEBOARD];
		Random rng = new Random();
		for (int i = 0; i < grid.length; i++) {
			int x = rng.nextInt(grid[i].length);
			for (int j = 0; j < grid[i].length; j++) {
				if (x == j) {
					grid[i][j] = Tile.HIDDEN_WALL;
				} else {
					grid[i][j] = Tile.BLANK;
				}	
			}
		}
	}
	public String toString() {
		String x = "";
		for (int i = 0; i < grid.length; i++) {
			for (int j = 0; j < grid[i].length; j++) {
				x += grid[i][j].getName() + " ";
			}
			x += "\n";
		}
		return x;
	}
	public int placeToken(int row, int column) {
		if (row < 0 || row > this.SIZEBOARD-1 || column < 0 ||  column > this.SIZEBOARD-1) {
			return -2;
		}
		if (grid[row][column] == Tile.CASTLE || grid[row][column] == Tile.WALL) {
			return -1;
		} else if (grid[row][column] == Tile.HIDDEN_WALL) {
			grid[row][column] = Tile.WALL;
			return 1;
		} else {
			grid[row][column] = Tile.CASTLE;
			return 0;
		}
	}
}